class I18n
  method_alias_chain :translate, :helper

  DEFAULTMEHOPTIONS= {helper: true}

  def translate_with_helper key, options
    options = DEFAULTMEHOPTIONS.merge(options)

    txt = translate_without_helper key, options

    if not session[:translating]
      txt
    else
      if options[:helper]
        "<span class='translateable' data-key='#{key}'>#{txt}</span>"
      else
        txt += " (#{key})"
      end
    end

    txt
  end
end
